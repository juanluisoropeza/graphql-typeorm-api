import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, BaseEntity } from 'typeorm';
import { Field, Int, ObjectType } from 'type-graphql';

@ObjectType()
@Entity()
export class Product extends BaseEntity {

  @Field()
  @PrimaryGeneratedColumn()
  id!: number; // el "!" se usa para decirle q siempre sera inicializado, pasa q al ser una clase, requiere q se haga un constructor para que se inicialice

  @Field(() => String)
  @Column()
  name!: string;

  @Field(() => Int)
  @Column('int', {default: 0})
  quantity!: number;

  @Field(() => String)
  @CreateDateColumn({ type: 'timestamp' })
  createdAt!: string;

}
