import 'reflect-metadata';
import { connect } from './config/typeorm'
import { startServer } from './server';

const main = async () => {
  connect();
  const app = await startServer();
  app.listen(3100);
  console.log('Server in port', 3100)
};

main();
